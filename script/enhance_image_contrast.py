# !/usr/bin/env python
# title           : enhance_image_contrast.py
# description     : Increases image contrast to make text clearer for img2text
# author          : Berat Onur Ersen (onurersen@gmail.com)
# date            : 20190218
# version         : 1.0
# usage           : python enhance_image_contrast.py <folder_path>
# notes           :
# python_version  : 3.6.5
# ==============================================================================
from PIL import Image, ImageEnhance
import traceback
import os
import sys
import shutil


def main():
    try:

        try:
            os.makedirs("../enhanced")
        except FileExistsError:
            print("cropped directory already exists, emptying...")
            shutil.rmtree("../enhanced", ignore_errors=True)
            os.makedirs("../enhanced")

        # takes session folder path as argument
        if len(sys.argv) == 1:
            input_folder = "../cropped"
        else:
            input_folder = sys.argv[1]

        files_to_be_enhanced = \
            [each_file for each_file in os.listdir(input_folder) if each_file.lower().endswith("png")]

        for file in files_to_be_enhanced:
            existing_image_path = os.path.join(input_folder, file)
            new_image_path = os.path.join("../enhanced",
                                          os.path.splitext(file)[0] + os.path.splitext(file)[1])
            image = Image.open(existing_image_path)
            enhanced_image = enhance_image_contrast(image)
            enhanced_image.save(new_image_path)
            print(existing_image_path + " enhanced.")

    except IOError:
        traceback.print_exc()


def enhance_image_contrast(image):
    enhancer = ImageEnhance.Contrast(image)
    enhanced_image = enhancer.enhance(7.0)
    return enhanced_image


if __name__ == '__main__':
    main()



