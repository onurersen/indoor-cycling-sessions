# !/usr/bin/env python
# title           : crop_session_images.py
# description     : Crops png images in target folder to prepare for contrast adjustment
# author          : Berat Onur Ersen (onurersen@gmail.com)
# date            : 20190218
# version         : 1.0
# usage           : python crop_session_images.py <folder_path>
# notes           :
# python_version  : 3.6.5
# ==============================================================================
from PIL import Image
import os
import sys
import shutil
import traceback


def main():
    try:

        try:
            os.makedirs("../cropped")
        except FileExistsError:
            print("cropped directory already exists, emptying...")
            shutil.rmtree("../cropped", ignore_errors=True)
            os.makedirs("../cropped")

        if len(sys.argv) == 1:
            input_folder = "../session"
        else:
            input_folder = sys.argv[1]

        files_to_be_enhanced = \
            [each_file for each_file in os.listdir(input_folder) if each_file.lower().endswith("png")]

        for file in files_to_be_enhanced:
            existing_image_path = os.path.join(input_folder, file)
            new_image_path = os.path.join("../cropped",
                                          os.path.splitext(file)[0] + os.path.splitext(file)[1])
            image = Image.open(existing_image_path)
            cropped_image = crop_numeric_area(image)
            cropped_image.save(new_image_path)
            print(existing_image_path + " cropped.")

    except IOError:
        traceback.print_exc()


def crop_numeric_area(image):
    (width, height) = image.size
    area = (0, height / 4 + height / 15, width, height - (height / 25))
    image = image.crop(area)
    return image


if __name__ == '__main__':
    main()
