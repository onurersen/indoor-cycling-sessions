# !/usr/bin/env python
# title           : convert_resize_images.py
# description     : converts png images in target folder to jpg
# author          : Berat Onur Ersen (onurersen@gmail.com)
# date            : 20190218
# version         : 1.0
# usage           : python convert_resize_images.py <folder_path>
# notes           :
# python_version  : 3.6.5
# ==============================================================================
from PIL import Image
import os
import sys
import traceback


def main():
    try:

        # takes session folder path as argument
        if len(sys.argv) == 1:
            folder = "../session"
        else:
            folder = sys.argv[1]

        files = [i for i in os.listdir(folder) if i.lower().endswith("png")]
        # iterate over files and crop to appropriate image size
        for file in files:
            existing_image_path = os.path.join(folder, file)
            new_image_path = os.path.join("../session",
                                          os.path.splitext(file)[0] + ".jpg")
            im = Image.open(existing_image_path)
            if not im.mode == 'RGB':
                im = im.convert('RGB')
            im.save(new_image_path, quality=20, optimize=True)
            print(existing_image_path + " converted to " + os.path.splitext(file)[0] + ".jpg")

    except IOError:
        traceback.print_exc()


if __name__ == '__main__':
    main()
